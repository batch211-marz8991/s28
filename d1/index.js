// Crud Operations
/*
-CRUD is acronym for: Create, Read, Update, Delete
-Create: The "Create" function allows user to create a new record in the database.
-Read: The "Read" function is similar to search function.
-Update: The "Update" function is used to modify existing record
-Delete: The "Delete" function allow users to remove records from database
*/

// CREATE: INSERT DOCUMENTS
/*
-The mongo shell uses Javascript for its syntax
*/
// INSERT ONE
db.users.insertOne({
			 firstName: "Jane",
			 lastName: "Doe",
			 age: 21,
			 contact: {
			 			 phone: "87654321",
				 email: "janedoe@gmail.com"
			 }
});
// INSERT MANY
db.users.insertMany([
{
			 firstName: "Stephen",
			 lastName: "Hawking",
			 age: 76,
				contact: {
						phone: "87654321",
			 			 email: "StephenHawking@gmail.com"
			 },
			 course: ["Phyton", "React", "Php"],
			 department: "none"
},
{
			 firstName: "Neil",
			 lastName: "Armstrong",
			 age: 82,
			 contact: {
			 			 phone: "87654321",
			 			 email: "neilarmstrong@gmail.com"
			 },
			 course: ["Phyton", "React", "Php"],
			 department: "none"
}

]);
// READ: FIND/RETRIEVE DOCUMENTS
/*
-the documents will be returned based on their order of storage in the collection
*/

db.users.find();


// FIND USING SINGLE PARAMETER
db.users.find({ firstName: "Stephen"});


// FIND USING MULTIPLE PARAMETERS
db.users.find({ lastName: "Armstrong", age: 82 });
//FIND : PRETTY METHOD
/*
-Allows us to be able to view the documents returned by our terminal in a "prettier" format.
*/ 
db.users.find({ lastName: "Armstrong", age: 82 }).pretty();
db.users.find().pretty();
// UPDATE: EDIT A DOCUMENT

// UPDATE ONE: Updating a single document
/*
Syntax:
db.collection
*/
db.users.insert({
			 firstName: "Test",
			 lastName: "Test",
			 age: 0,
			 contact: {
			 			 phone: "00000000",
			 			 email: "test@gmail.com"
			 },
			 courses: [],
			 department: "none"
});
// Mini Activity1
db.users.updateOne(
{
			 firstName: "Test"
},
{
$set: {
			 firstName: "Bill",
			 lastName: "Gates",
			 age: 65,
			 contact: {
			 			 phone: "87654321",
			 			 email: "bill@gmail.com"
			 },
			 course: [ "PHP", "Laravel", "HTML"],
			 department: "Operations",
			 status: "active"
			 }
});

db.users.find({firstName: "Bill"});
// UPDATE MANY
db.users.updateMany(
			 { department: "none" },
			 {
			 $set: {
			 					 department: "HR"
			 				 }
			 }
);

db.users.find().pretty();
// REPLACE ONE -- replaces the whole document

db.users.replaceOne(
{ firstName: "Bill" },
{
			 firstName: "Bill",
			 lastName: "Gates",
			 age: 65,
			 contact: {
			 		 phone: "12345678",
			 		 email: "bill@gmail.com"
			 },
		 course: [ "PHP", "Laravel", "HTML"],
		 department: "Operations",
		 }
);
// DELETE: DELETING DOCUMENTS
db.users.insertOne({
		 firstName: "test"
});
// deleteOne
db.users.deleteOne({
		 firstName: "test"
});

db.users.find({firstName: "test"}).pretty();
// DELETE MANY -- delete many documents
db.users.insertMany([
{
		 firstName: "Bill",
		 lastName: "Magalang",
		 age: 12
},
{
		 firstName: "Bill",
		 lastName: "Batumbakal",
		 age: 13
}
]);

db.users.find({firstName: "Bill"});
// deleteMany -- soft delete(Recommended)
db.users.deleteMany({  firstName: "Bill"
});
db.users.find({firstName: "Bill"}) //delete lahat ng may name na "Bill"
// DELETE ALL: Delete all document -- not good practice.
// Advance Queries


// Query an embedded document
db.users.find({
 	ontact: {
			 phone: "87654321",
			 email: "StephenHawking@gmail.com"
 	}
}).pretty();
// Query on nested field
db.users.find({
		 "contact.email": "StephenHawking@gmail.com"
}).pretty();

// Query an array with exact elements
db.users.find({course: {$all: ["React", "Phyton"]}}).pretty();
// Query an embedded array
db.users.insertOne({
			 nameArr: [
			 {
			 			 nameA: "Juan"
			 },
			 {
			 			 nameB: "Tamad"
			 }
			 ]
});

db.users.find({
			 nameArr:
			 {
			 nameA: "Juan"
			 }
}).pretty();
