

// insert single room
db.rooms.insertOne({
    name: "Single",
    accomodates: 2,
    price: 1000,
    description: "A simple room with all the basic necessities.",
    rooms_available: 10,
    isAvailable: false
});


// insert multiple rooms
db.rooms.insertMany([
   {
        name: "Double",
        accomodates: 3,
        price: 2000,
        description: "A room fit for a small family going on a vacation.",
        rooms_available: 5,
        isAvailable: false 
    },

    {
        name: "Queen",
        accomodates: 4,
        price: 4000,
        description: "A room with a queen sized bed perfect for a simple getaway.",
        rooms_available: 15,
        isAvailable: false 
    }

]);


// Search for a room with a name double
db.rooms.find({ name: "Double" });


//update the queen room using updateOne method
db.rooms.updateOne(
{ 
    name: "Queen"
},
{
    $set: {
        rooms_available: 0 
    }
}
);


// delete rooms that have 0 availability
db.rooms.deleteMany({ rooms_available: 0 });